#include <memory>
#include <string>
#include <random>
#include <math.h>
#include <iostream>
#include <fstream>

using namespace std;

vector<double> operator*(const vector<double> &, const vector<double> &);

vector<double> operator-(const vector<double> &, const vector<double> &);

double randDouble(double lowBound, double highBound, double decPlaces);

void vectorInitRand(vector<double> &v, double lowBound, double highBound, double decPlaces);

string vectorToString(const vector<double> &v);

string vectorToString(const vector<int> &v);

double sumAllElems(const vector<double> &v);

class neuron;

class neuralNet
{

    double (*actFunc)(double);
    vector<int> neuronsByLayer;
    double beta;
    double derivPrecision;
    static int count;
    vector<vector<shared_ptr<neuron>>> neuronMatrix;

public:
    neuralNet(vector<int> neuByLayer, double (*actFunc)(double), double derivPrecision = 0.000001, double beta = 1, double wLowBound = -1, double wHighBound = 1, double wDecPlaces = 8, double bLowBound = -2, double bHighBound = 2, double bDecPlaces = 8);

    ~neuralNet();

    vector<double> predict(vector<double> input) const;

    double getCost(vector<double> output, vector<double> desired_out) const;

    vector<double> getNeuronWDeriv(neuron &N, vector<double> input, vector<double> desired_out) const;

    double getNeuronBDeriv(neuron &N, vector<double> input, vector<double> desired_out) const;

    void train(vector<double> input, vector<double> desired_out);

    void save(string s);

    void load(string s);

    int getLayerCount() const;

    string layerToString(int l) const;

    void breed(neuralNet &net1, neuralNet &net2);
};

class neuron
{
    vector<double> weights;
    double bias;
    double (*actFunc)(double);
    vector<weak_ptr<neuron>> prevConV;

    friend vector<double> neuralNet::getNeuronWDeriv(neuron &, vector<double>, vector<double>) const;
    friend double neuralNet::getNeuronBDeriv(neuron &, vector<double>, vector<double>) const;
    friend void neuralNet::train(vector<double> input, vector<double> desired_out);
    friend vector<double> neuralNet::predict(vector<double> input) const;

public:
    neuron(vector<double> w, double b, vector<weak_ptr<neuron>> prevConV, double (*actFunc)(double));

    ~neuron();

    double getActValue() const;

    vector<double> &getWeightsRef();

    double &getBiasRef();

    //vector<weak_ptr<neuron>> &getPrevConRef();

    string toString() const;

    string prevConVtoString() const;

    void breed(neuron &neuron1, neuron &neuron2);
};
