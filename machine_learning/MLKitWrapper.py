from ctypes import *
import glob

libfile = glob.glob('build/*/MLKit*.so')[0]

lib = cdll.LoadLibrary(libfile)

# defining return and argument types for the imported c functions
lib.neuralNet_new.argtypes = [POINTER(
    c_int), c_int, c_double, c_double, c_double, c_double, c_double, c_double, c_double, c_double]
lib.neuralNet_new.restype = c_void_p

lib.neuralNet_predict.argtypes = [
    c_void_p, POINTER(c_double), c_int, POINTER(c_double)]
lib.neuralNet_predict.restype = POINTER(c_double)

lib.neuralNet_train.argtypes = [c_void_p, POINTER(
    c_double), c_int, POINTER(c_double), c_int]


lib.neuralNet_getCost.argtypes = [c_void_p, POINTER(
    c_double), c_int, POINTER(c_double), c_int]
lib.neuralNet_getCost.restype = c_double

lib.neuralNet_save.argtypes = [c_void_p, c_char_p]
lib.neuralNet_load.argtypes = [c_void_p, c_char_p]

# NeuralNet wrapper class


class neuralNet:
    def __init__(self, neuronsByLayer, derivPrecision=0.0000001, beta=1, wLowBound=-10, wHighBound=10, wDecPlaces=7, bLowBound=-100, bHighBound=100,  bDecPlaces=3):
        self.neuronsByLayer = neuronsByLayer

        arg1 = (c_int * len(neuronsByLayer))(*neuronsByLayer)
        arg2 = c_int(len(neuronsByLayer))
        arg3 = c_double(derivPrecision)
        arg4 = c_double(beta)
        arg5 = wLowBound
        arg6 = wHighBound
        arg7 = wDecPlaces
        arg8 = bLowBound
        arg9 = bHighBound
        arg10 = bDecPlaces
        self.obj = lib.neuralNet_new(
            arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10)

    def __del__(self):
        lib.neuralNet_delete(self.obj)

    def predict(self, inputV):
        if len(inputV) != self.neuronsByLayer[0]:
            raise Exception("Input array length must be:" +
                            str(self.neuronsByLayer[0]))

        arg1 = self.obj
        arg2 = (c_double * len(inputV))(*inputV)
        arg3 = c_int(len(inputV))
        arg4 = (c_double*self.neuronsByLayer[-1])()
        returnList = []
        lib.neuralNet_predict(arg1, arg2, arg3, arg4)
        for i in range(self.neuronsByLayer[-1]):
            returnList.append(arg4[i])
        return returnList

    def train(self, inputV, desiredOutputV):
        if len(inputV) != self.neuronsByLayer[0]:
            raise Exception("Input array length must be:" +
                            str(self.neuronsByLayer[0]))

        if len(desiredOutputV) != self.neuronsByLayer[-1]:
            raise Exception("Desired output array length must be:" +
                            str(self.neuronsByLayer[-1]))

        arg1 = self.obj
        arg2 = (c_double * len(inputV))(*inputV)
        arg3 = c_int(len(inputV))
        arg4 = (c_double * len(desiredOutputV))(*desiredOutputV)
        arg5 = c_int(len(desiredOutputV))
        lib.neuralNet_train(arg1, arg2, arg3, arg4, arg5)

    def getCost(self, outputV, desiredOutputV):
        if len(outputV) != self.neuronsByLayer[-1]:
            raise Exception("Output array length must be:" +
                            str(self.neuronsByLayer[-1]))

        if len(desiredOutputV) != self.neuronsByLayer[-1]:
            raise Exception("Desired output array length must be:" +
                            str(self.neuronsByLayer[-1]))

        arg1 = self.obj
        arg2 = (c_double * len(outputV))(*outputV)
        arg3 = c_int(len(outputV))
        arg4 = (c_double * len(desiredOutputV))(*desiredOutputV)
        arg5 = c_int(len(desiredOutputV))
        return (lib.neuralNet_getCost(arg1, arg2, arg3, arg4, arg5))

    def save(self, filename):
        arg1 = self.obj
        arg2 = filename.encode("utf-8")
        lib.neuralNet_save(arg1, arg2)

    def load(self, filename):
        arg1 = self.obj
        arg2 = filename.encode("utf-8")
        lib.neuralNet_load(arg1, arg2)

    def breed(self, net1, net2):
        arg1 = self.obj
        arg2 = net1
        arg3 = net2
        lib.neuralNet_breed(arg1, arg2, arg3)
