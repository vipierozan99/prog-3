MACHINE LEARNING KIT

Made in c++ for speed, but is wrapped in python for ease of use.
Compile the library with:
   $ pyhton3 setup.py build

Import the file MLKitWrapper.py then use the neuralNet class.

example files: 
    gameOpt.py (a game optimization example using the genetic algorithm i.e. breed() )
    MLKitTest.py (a classificator network example for flowers using the iris.dat dataset and the gradient descent algorithm i.e. train() )

class neuralNet:
    __init__(self, neuronsByLayer, derivPrecision=0.0000001, beta=1)

        neuronsByLayer = a list of integers representing the neuron layer structure of the net, being the index=0 the input layer and the last index the output layer.

        derivPrecision = a float 'h' used in the derivation formula -> f'(x) = (f(x+h)-f(x))/h

        beta = a float used for adjusting the training speed, it multiplies the partial derivative for backpropagation. (use values very close to 1 e.g. 1.0005)


    predict(self, inputV)
        #predicts an output based on an input

        inputV = a list of floats the same size as the input layer (neuronsBylayer[0])

        returns a list of floats from 0-1 the same size as the output layer (neuronsByLayer[-1])

    train(self, inputV, desiredOutputV)
        #trains the network based on an input and a desired output

        inputV = a list of floats the same size as the input layer (neuronsBylayer[0])

        desiredOutputV = a list of floats from 0-1 the same size as the output layer (neuronsByLayer[-1]) containing the correct answer

        returns nothing

    save(self, filename)
        #saves the network self (weights, biases, etc)
        filename = a string with the file name to save the neuralNet current state to(will create/overwrite!!)

    load(self, filename)
        #loads a network into self from a file
        filename = a string with the file name to load a neuralNet state from


    breed(self, net1, net2)
        #breeds net1 and net2 and writes it to self
        net1,net2 = the neuralNets that will be used for breeding 
