import pygame
import random
from pygame.locals import *
from operator import itemgetter

import MLKitWrapper as ml

# Player class the inherits from the pygame Sprite Class


class Player(pygame.sprite.Sprite):
    def __init__(self, brain, idNum):
        super(Player, self).__init__()
        self.surf = pygame.Surface((25, 25))
        self.surf.fill((random.randint(0, 255), random.randint(
            0, 255), random.randint(0, 255)))
        #self.surf = pygame.image.load("lala.jpg")
        self.rect = self.surf.get_rect(center=(40, 400))
        self.speed = [0, 0]
        self.accel = [0, -2]
        self.brain = brain  # the neuralNet
        self.idNum = idNum

    def jump(self, impulsePower):
        if self.rect.bottom >= 500:
            self.speed[1] = impulsePower*30

    # Physics calculations
    def update(self, pressed_keys, MLData):
        self.speed[0] += self.accel[0]
        self.speed[1] += self.accel[1]
        self.rect.move_ip(-self.speed[0], -self.speed[1])

        if pressed_keys[K_UP]:
            self.jump(1)

        shouldJump = self.brain.predict(
            [MLData["Xcoord1"], MLData["Ycoord1"], MLData["Xcoord2"], MLData["Ycoord2"]])[0]

        if shouldJump > 0.2:  # cant jump in air, so this is ok
            self.jump(shouldJump)

        # removed side slide
        # if pressed_keys[K_LEFT]:
            #self.rect.move_ip(-10, 0)
        # if pressed_keys[K_RIGHT]:
            #self.rect.move_ip(10, 0)

        # keep player in screen above ground
        if self.rect.left < 0:
            self.rect.left = 0
        elif self.rect.right > 800:
            self.rect.right = 800
        if self.rect.top <= 0:
            self.rect.top = 0
        elif self.rect.bottom >= 500:
            self.rect.bottom = 500

# Enemy class that inherits from pygame's Sprite class


class Enemy(pygame.sprite.Sprite):
    def __init__(self, Xcenter=random.randint(800, 1050)):
        super(Enemy, self).__init__()
        self.height = random.randint(30, 75)
        self.surf = pygame.Surface((20, self.height))
        self.surf.fill((255, 255, 255))
        self.rect = self.surf.get_rect(
            center=(Xcenter, 500-(self.height/2))
        )
        self.speed = -10

    def update(self):
        self.rect.move_ip(self.speed, 0)
        if self.rect.right < 0:
            self.kill()


class ScreenText():
    def __init__(self, text, x, y):
        self.font = pygame.font.Font('freesansbold.ttf', 32)
        self.text = self.font.render(text, True, (255, 255, 255), (0, 0, 0))
        self.textRect = self.text.get_rect()
        self.textRect.center = (x//2, y//2)

    def update(self, text):
        self.text = self.font.render(text, True, (255, 255, 255), (0, 0, 0))


pygame.init()
FPS = 120
fpsclock = pygame.time.Clock()

screen = pygame.display.set_mode((800, 600))

# add enemy timer, frequency increases with score
ADDENEMY = pygame.USEREVENT + 1
initialFreq = 60000
pygame.time.set_timer(ADDENEMY, int(initialFreq/FPS))

score = 0
individuals = []
generationSize = 300
fitnessVector = [0]*generationSize  # each players score

for i in range(generationSize):
    ind = ml.neuralNet([4, 3, 1])
    individuals.append(Player(ind, i))


ground = pygame.Surface((800, 100))
ground.fill((0, 255, 0))

enemies = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
players = pygame.sprite.Group()
generation = 0

difficulty = ScreenText("Difficulty: 100%", 255, 50)
scoreDisplay = ScreenText("Score: 0", 130, 120)
generationDisplay = ScreenText("Generation: 0", 220, 190)
exit = False

while True:
    score = 0
    difficulty.update("Difficulty: 100%")
    scoreDisplay.update("Score: 0")
    pygame.time.set_timer(ADDENEMY, int(initialFreq/FPS))
    print("Generation:", generation)
    generationDisplay.update("Generation: "+str(generation))

    for i in range(generationSize):
        all_sprites.add(individuals[i])
        players.add(individuals[i])

    # collecting the x,y coordinates from the 2 closest enemies to feed the neuralNet
    MLData = {
        "Ycoord1": 0,
        "Xcoord1": 0,
        "Ycoord2": 0,
        "Xcoord2": 0
    }

    running = True
    if exit:
        break
    while running:
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
                    exit = True
            elif event.type == QUIT:
                running = False
            elif(event.type == ADDENEMY):
                new_enemy = Enemy()
                if random.randint(0, 50) == 0:
                    new_enemy2 = Enemy(Xcenter=new_enemy.rect.center[0]+50)
                    enemies.add(new_enemy2)
                    all_sprites.add(new_enemy2)
                enemies.add(new_enemy)
                all_sprites.add(new_enemy)

        enemies.update()

        # sorting enemies by distance to y=0
        enemiesSortByRectLeft = enemies.sprites()
        enemiesSortByRectLeft.sort(key=lambda x: x.rect.left)
        MLData["Xcoord1"] = 0
        MLData["Ycoord1"] = 0
        MLData["Xcoord2"] = 0
        MLData["Ycoord2"] = 0
        first = 0
        if len(enemiesSortByRectLeft) > 0:
            # excluding those with x<40 i.e. behind the player
            while first < len(enemiesSortByRectLeft) and enemiesSortByRectLeft[first].rect.left < 40:
                first += 1

        if first < len(enemiesSortByRectLeft):
            MLData["Xcoord1"] = (enemiesSortByRectLeft[first].rect.left-40)/760
            MLData["Ycoord1"] = (enemiesSortByRectLeft[first].rect.top-425)/45
        if first+1 < len(enemiesSortByRectLeft):
            MLData["Xcoord2"] = (
                enemiesSortByRectLeft[first+1].rect.left-40)/760
            MLData["Ycoord2"] = (
                enemiesSortByRectLeft[first+1].rect.top-425)/45

        # print(MLData)

        pressed_keys = pygame.key.get_pressed()

        for player in players.sprites():
            player.update(pressed_keys, MLData)

        # drawing the screen
        screen.fill((0, 0, 0))
        screen.blit(ground, (0, 500))
        screen.blit(player.surf, player.rect)
        screen.blit(difficulty.text, difficulty.textRect)
        screen.blit(scoreDisplay.text, scoreDisplay.textRect)
        screen.blit(generationDisplay.text, generationDisplay.textRect)

        for entity in all_sprites:
            screen.blit(entity.surf, entity.rect)

        # if the player touches any enemy, player dies
        for player in players.sprites():
            if pygame.sprite.spritecollideany(player, enemies):
                print("colision! player:", player.idNum, "score:", score)
                fitnessVector[player.idNum] = score
                player.kill()

        if not bool(players):
            running = False

        score += 1
        scoreDisplay.update("Score: "+str(score))

        # every 300 score points, the enemy spawn frequency updates
        if score % 300 == 0:
            pygame.time.set_timer(ADDENEMY, int(initialFreq/FPS)-int(score/40))
            print("score", score)
            print("speed", '%.3f' % (int(initialFreq*100/FPS) /
                                     (int(initialFreq/FPS)-int(score/40))), "%")
            difficulty.update("Difficulty: "+'%.3f' %
                              (initialFreq*100/(initialFreq-int(score/40)))+"%")

        pygame.display.flip()

        fpsclock.tick(FPS)

    for entity in all_sprites:
        entity.kill()

    # THE GENETIC ALGORITHM #
    # pair up each player with it's score
    individuals = list(zip(individuals, fitnessVector))
    # sort for the ones with the best score first
    individuals.sort(key=itemgetter(1), reverse=True)
    offspringBrains = []
    for i in range(generationSize):
        # creates a random net
        # breed the new population with the best player from the last generation
        if(i > (generationSize - generationSize/5) and i != 0):
            offspringBrainsBuf = ml.neuralNet([4, 3, 1])
            offspringBrainsBuf.breed(
                individuals[0][0].brain.obj, offspringBrainsBuf.obj)
            offspringBrains.append(offspringBrainsBuf)
            continue

        if(i > generationSize/2 and i != 0):
            offspringBrainsBuf = individuals[i][0].brain
            offspringBrainsBuf.breed(
                individuals[0][0].brain.obj, offspringBrainsBuf.obj)
            offspringBrains.append(offspringBrainsBuf)
            continue

        if(i != 0):
            offspringBrainsBuf = individuals[i][0].brain
            offspringBrainsBuf.breed(
                individuals[0][0].brain.obj, offspringBrainsBuf.obj)
            offspringBrains.append(offspringBrainsBuf)
            continue

        # but keep the best one from the last gen
        if(i == 0):
            offspringBrainsBuf = individuals[0][0].brain
            offspringBrains.append(offspringBrainsBuf)
            continue

    # Put the brains into new players
    individuals = []
    for i in range(generationSize):
        individuals.append(Player(offspringBrains[i], i))
    fitnessVector = [0]*generationSize
    generation += 1
    # Rerun game
    # That's it folks!
