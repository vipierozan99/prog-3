import MLKitWrapper as ml
import math
import random


def getTargetVector(string):
    if string == "Iris-setosa\n":
        return [1, 0, 0]
    if string == "Iris-versicolor\n":
        return [0, 1, 0]
    if string == "Iris-virginica\n":
        return [0, 0, 1]


"""def getTargetVector(i):
	i=int(i)
	listofzeros = [0] * 5
	listofzeros[i-4] = 1
	return listofzeros"""


iris_file = open("iris.data", "r")

trainingData = []
counter = 0

for line in iris_file.readlines():
    tokens = line.split(",")
    # print(tokens)
    if tokens[0] == '\n':
        break
    trainingData.append([[float(i)/10.0 for i in tokens[0:4]],
                         getTargetVector(tokens[4])])
    counter += 1

totalLines = counter
# print(trainingData)


for rerun in range(6):
    myNet = ml.neuralNet([4, 4, 3], 0.00000001, 0.9, wLowBound=0, wHighBound=0,
                         wDecPlaces=4, bLowBound=0, bHighBound=0,  bDecPlaces=4)
    myNet.load("myNet.dat")
    counter = 0
    random.shuffle(trainingData)

    for retrain in range(1):
        counter = 0
        for data in trainingData[0:-40]:
            myNet.train(data[0], data[1])
            prediction = myNet.predict(data[0])
            cost = myNet.getCost(prediction, data[1])
            # print("training:", str(counter), '/', str(totalLines))
            # print("prediction =", prediction)
            # print("target=", data[1])
            # print("cost", cost)
            # print()
            counter += 1

    counter = 1
    totalLines = 27
    costList = []
    accuracy = 0
    attempts = 0
    for data in trainingData[-40:-1]:
        prediction = myNet.predict(data[0])
        cost = myNet.getCost(prediction, data[1])
        # print("testing:", str(counter), '/', str(totalLines))
        # print("prediction =", prediction)
        # print("target=", data[1])
        # print("cost", cost)
        # print()
        costList.append(cost)
        if(prediction.index(max(prediction)) == data[1].index(max(data[1]))):
            accuracy += 1
        counter += 1
        attempts += 1

    cost = sum(costList)/len(costList)

    print("average cost:", cost)
    print("accuracy:", accuracy, '/', attempts,
          "   ", accuracy/attempts*100, '%')

myNet.save("myNet.dat")
