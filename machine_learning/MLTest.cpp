#include "MLKit.hpp"
#include <iostream>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <random>
using namespace std;

int getHighestIndex(vector<double> v)
{
    int hIndex = 0;
    for (int i = 0; i < v.size(); i++)
    {
        if (v[i] > v[hIndex])
        {
            hIndex = i;
        }
    }
    return hIndex;
}

double Sigmoid(double x)
{
    return 1.0 / (1 + exp(-x));
}

vector<double> getTargetVector(string x)
{
    vector<double> v;
    v.resize(3, 0);
    if (x.compare("Iris-virginica") == 0)
    {
        v[2] = 1;
    }
    if (x.compare("Iris-versicolor") == 0)
    {
        v[1] = 1;
    }
    if (x.compare("Iris-setosa") == 0)
    {
        v[0] = 1;
    }
    return v;
}

int main()
{

    fstream ip("iris.data");
    string s;
    getline(ip, s);

    vector<vector<double>> trainingData0;
    vector<double> buffer;
    vector<vector<double>> target0;
    string delimiter;
    size_t pos;
    string token;
    for (int i = 0; i < 148 && ip.good(); i++)
    {
        getline(ip, s);
        //cout << s << endl;
        delimiter = ",";
        pos = 0;

        while ((pos = s.find(delimiter)) != string::npos)
        {
            token = s.substr(0, pos);
            buffer.push_back(stod(token) / 10.0);
            s.erase(0, pos + delimiter.length());
        }
        trainingData0.push_back(buffer);
        buffer.clear();

        target0.push_back(getTargetVector(s));
        if (!ip.good())
            break;
    }

    vector<int> indexes;
    indexes.reserve(target0.size());
    for (int i = 0; i < target0.size(); ++i)
        indexes.push_back(i);
    random_shuffle(indexes.begin(), indexes.end());

    vector<vector<double>> trainingData;
    vector<vector<double>> target;

    for (vector<int>::iterator it1 = indexes.begin(); it1 != indexes.end(); ++it1)
    {
        trainingData.push_back(trainingData0[*it1]);
        target.push_back(target0[*it1]);
    }
    // for (auto &d : trainingData)
    //     cout << vectorToString(d) << endl;
    neuralNet *myNet = new neuralNet(vector<int>{4, 4, 3}, Sigmoid, 0.00001, 0.99);

    for (int i = 0; i < myNet->getLayerCount(); i++)
    {
        cout << myNet->layerToString(i) << endl;
    }

    double a = myNet->neuronMatrix[1][0]->getActValue();
    //myNet->save("myNet->dat");
    //return 0;
    vector<double> prediction;
    vector<double> costAverage1;
    vector<double> costAverage2;
    vector<double> gotRight;
    double currentCost;
    cout << endl
         << "TESTING" << endl;
    for (int i = 120; i < 148; i++)
    {
        prediction = myNet->predict(trainingData[i]);
        currentCost = myNet->getCost(prediction, target[i]);
        cout << "Prediction:" << vectorToString(prediction) << endl
             << "Target:    " << vectorToString(target[i]) << endl
             << "Cost:" << currentCost << endl
             << endl;
        costAverage1.push_back(currentCost);
    }
    for (int reTrain = 0; reTrain < 10; reTrain++)
    {
        for (int i = 0; i < 120; i++)
        {
            myNet->train(trainingData[i], target[i]);
            cout << "Training:" << i + 1 << "/120" << endl;
            prediction = myNet->predict(trainingData[i]);
            cout << "Prediction:" << vectorToString(prediction) << endl
                 << "Target:" << vectorToString(target[i]) << endl
                 << "Cost:" << myNet->getCost(prediction, target[i]) << endl
                 << endl;
        }
    }

    cout << endl
         << "TESTING" << endl;
    for (int i = 120; i < 148; i++)
    {
        prediction = myNet->predict(trainingData[i]);
        currentCost = myNet->getCost(prediction, target[i]);
        cout << "Prediction:" << vectorToString(prediction) << endl
             << "Target:    " << vectorToString(target[i]) << endl
             << "Cost:" << currentCost << endl
             << endl;
        costAverage2.push_back(currentCost);
        if (getHighestIndex(prediction) == getHighestIndex(target[i]))
        {
            gotRight.push_back(1);
        }
        else
        {
            gotRight.push_back(0);
        }
    }

    cout << "Cost average:" << endl
         << "Before training:" << sumAllElems(costAverage1) / costAverage1.size() << endl
         << "After training: " << sumAllElems(costAverage2) / costAverage2.size() << endl
         << "Got right:" << sumAllElems(gotRight) << "/" << gotRight.size() << endl;

    //myNet->save("myNet.dat");
    delete myNet;
    return 0;
}
