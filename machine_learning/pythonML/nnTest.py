import nn
import numpy as np

layer_sizes = (3,5,10)
x = np.ones((layer_sizes[0],1))

net = nn.NeuralNetwork(layer_sizes)
prediction = net.predict(x)
print(prediction)
net.trainGradDescent([0.5,0.6,0.7],[0,1,0])
