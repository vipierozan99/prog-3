import numpy as np

class NeuralNetwork:

	def __init__(self, layer_sizes):
		weight_shapes = [(a,b) for a,b in zip(layer_sizes[1:],layer_sizes[:-1])]
		self.weights = np.array([np.random.standard_normal(s)/s[1]**.5 for s in weight_shapes])
		self.biases = [np.zeros((s,1)) for s in layer_sizes[1:]]
		print(self.biases)
		self.h = 0.000000001

	def predict(self, a):
		for w,b in zip(self.weights,self.biases):
			a = self.activation(np.matmul(w,a) + b)
		return a

	def getCost(self, data, label):
		return sum([(a-b)**2 for (a,b) in zip(data,label)])/len(data)

	def getAvgCost(self, data, labels):
		return sum([self.getCost(a,b) for (a,b) in zip(data,labels)])/len(data)

	def trainGradDescent(self, data, label):
		for i in range(len(self.weights)):
			for j in range(len(self.weights[i])):
				for k in range(len(self.weights[i][j])):
					fOfX = self.getCost(data,label)
					self.weights[i][j][k] += self.h
					fOfXPlusH = self.getCost(data,label)
					deriv = (fOfX - fOfXPlusH)/self.h
					self.weights[i][j][k] -= deriv + self.h
		for i in range(len(self.biases)):
			for j in range(len(self.biases[i])):
				fOfX = self.getCost(data,label)
				self.biases[i][j] += self.h
				fOfXPlusH = self.getCost(data,label)
				deriv = (fOfX - fOfXPlusH)/self.h
				self.biases[i][j] -= deriv + self.h
	
	def gradDescent(self, data, labels);
		for i in range(len(labels)):
			self.trainGradDescent(data[i],label[i])
		

	@staticmethod
	def activation(x):
		return 1/(1+np.exp(-x))
