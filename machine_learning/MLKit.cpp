#include "MLKit.hpp"
#include <ctime>

using namespace std;

int neuralNet::count = 0;

//Overload of * for 2 vectors, element by element
vector<double> operator*(const vector<double> &A, const vector<double> &B)
{
    vector<double> C;
    for (int i = 0; i < int(A.size()); i++)
    {
        C.push_back(A[i] * B[i]);
    }
    return C;
}

//Overload of * for 2 vectors, element by element
vector<double> operator-(const vector<double> &A, const vector<double> &B)
{
    vector<double> C;
    for (int i = 0; i < int(A.size()); i++)
    {
        C.push_back(A[i] - B[i]);
    }
    return C;
}

double randDouble(double lowBound, double highBound, double decPlaces)
{
    if (!abs(lowBound - highBound))
        return 0;
    double r=(rand() % int(pow(10, decPlaces) * abs(lowBound - highBound)) + pow(10, decPlaces) * lowBound) / pow(10, decPlaces);
	//cout << '['<<lowBound<<','<< highBound<<"]: "<< r <<" decP:"<< decPlaces <<endl;
	return r;
}

// Randomizes the vector
void vectorInitRand(vector<double> &v, double lowBound, double highBound, double decPlaces)
{
    for (int i = 0; i < int(v.size()); i++)
    {
        v[i] = randDouble(lowBound, highBound, decPlaces);
    }
}

// Vector to string, for debugging
string vectorToString(const vector<double> &v)
{
    string s;
    for (auto &d : v)
    {
        s += to_string(d) + ',';
    }
    s.pop_back();
    return s;
}
// Vector to string, for debugging
string vectorToString(const vector<int> &v)
{
    string s;
    for (auto &d : v)
    {
        s += to_string(d) + ',';
    }
    return s;
}

// Sum all the elements in a vector
double sumAllElems(const vector<double> &v)
{
    double sum = 0;
    for (auto &d : v)
    {
        sum += d;
    }
    return sum;
}

neuron::neuron(vector<double> w, double b, vector<weak_ptr<neuron>> prevConV, double (*actFunc)(double))
{
    this->weights = w;
    this->bias = b;
    this->prevConV = prevConV;
    this->actFunc = actFunc;
}

neuron::~neuron()
{
    //cout << "neuron destructor called." << endl;
}

// Calculate the activation value of a neuron, which is all the actValues from the previous layer, weighted and summed up, plus the bias, all going into the activation function
double neuron::getActValue() const
{
    unique_ptr<vector<double>> prevActValuesPtr(new vector<double>);
    if (!prevConV.size())
    {
        return bias;
    }
    int neruonCounter = 0;
    for (auto &neuronPtr : prevConV)
    {
        prevActValuesPtr->push_back(neuronPtr.lock()->getActValue());
        neruonCounter++;
    }
    unique_ptr<vector<double>> weightedPrevs(new vector<double>((*prevActValuesPtr) * weights));
    return actFunc(sumAllElems(*weightedPrevs) + bias);
}

//Weights and bias of a neuron to a string, for debugging
string neuron::toString() const
{
    string s = string("<w:");
    for (auto &w : weights)
    {
        s += to_string(w) + ",";
    }
    s.back() = '/';
    s += "/b:" + to_string(bias) + '>';
    return s;
}

//All the previous layer neurons toString()
string neuron::prevConVtoString() const
{
    string s = string("<");
    for (auto &pc : prevConV)
    {
        if (pc.lock())
        {
            s += pc.lock()->toString() + ",";
        }
        else
        {
            s += "NULL,";
        }
    }
    if (s.size() > 1)
    {

        s.back() = '>';
    }
    else
    {
        s.push_back('>');
    }
    return s;
}
//Getter/Setter for a neuron's weights, internal use only
vector<double> &neuron::getWeightsRef()
{
    return this->weights;
}
//Getter/Setter for a neuron's bias, internal use only
double &neuron::getBiasRef()
{
    return this->bias;
}

// Constructor for a neural Net
neuralNet::neuralNet(vector<int> neuByLayer, double (*actFunc)(double), double derivPrecision, double beta, double wLowBound, double wHighBound, double wDecPlaces, double bLowBound, double bHighBound, double bDecPlaces)
{
    this->actFunc = actFunc;
    this->neuronsByLayer = neuByLayer;
    this->neuronMatrix = vector<vector<shared_ptr<neuron>>>();
    this->derivPrecision = derivPrecision;
    this->beta = beta;
    if (count == 0)
        srand(time(NULL));
    this->count++; //neuralNet static attribute for counting, used for initializing srand() only once.

    vector<shared_ptr<neuron>> layer;
    vector<double> weights;
    vector<weak_ptr<neuron>> prevConnects;
    double bias = 0;

    //first neuron layer is the input layer, has no previous layer
    for (int i = 0; i < neuronsByLayer[0]; i++)
    {
        vectorInitRand(weights, wLowBound, wHighBound, wDecPlaces);
        bias = randDouble(bLowBound, bHighBound, bDecPlaces);
        layer.push_back(shared_ptr<neuron>(new neuron(weights, bias, prevConnects, actFunc)));
    }
    neuronMatrix.push_back(layer);
    layer.clear();

    // all the other layers
    for (int i = 1; i < int(neuronsByLayer.size()); i++)
    {
        weights.resize(neuronsByLayer[i - 1]);
        for (int j = 0; j < neuronsByLayer[i]; j++)
        {
            vectorInitRand(weights, wLowBound, wHighBound, wDecPlaces);
            bias = randDouble(bLowBound, bHighBound, bDecPlaces);
            for (int k = 0; k < int(neuronMatrix[i - 1].size()); k++)
            {
                prevConnects.emplace_back(neuronMatrix[i - 1][k]);
            }
            layer.push_back(shared_ptr<neuron>(new neuron(weights, bias, prevConnects, actFunc)));
            prevConnects.clear();
        }
        neuronMatrix.push_back(layer);
        layer.clear();
    }
}

neuralNet::~neuralNet()
{
    //cout << "neuralNet destructor called." << endl;
}

// Gets the network's output for a given input, calculates all the actValues from last to first, recusively
vector<double> neuralNet::predict(vector<double> input) const
{
    vector<double> prediction;
    //input layer biases are used for storing input, it's activation function just returns the bias
    for (int i = 0; i < int(input.size()); i++)
    {
        neuronMatrix.front()[i]->bias = input[i];
    }
    int neuronCounter = 0;
    for (auto &neuron : neuronMatrix.back())
    {
        prediction.push_back(neuron->getActValue());
        neuronCounter++;
    }
    return prediction;
}

// Calculates the network cost function for a given output and the correct output, it's basically the variance
double neuralNet::getCost(vector<double> output, vector<double> desired_out) const
{
    double cost = 0;
    for (int i = 0; i < int(output.size()); i++)
    {
        cost += pow(output[i] - desired_out[i], 2);
    }
    return (cost / output.size());
}

// Returns a vector with all the network's cost function partial derivatives acording to a neurons weights
vector<double> neuralNet::getNeuronWDeriv(neuron &N, vector<double> input, vector<double> desired_out) const
{
    vector<double> deriv;
    vector<double> output = this->predict(input);
    double partDeriv;
    for (int i = 0; i < int(N.weights.size()); i++)
    {
        partDeriv = 0;
        //using the definition of derivative, with the limit not being 0, but derivPrecision, which should be close to 0 i.e  (f(x+h) - f(x))/h
        N.weights[i] += derivPrecision;
        partDeriv = (this->getCost(this->predict(input), desired_out) - this->getCost(output, desired_out)) / derivPrecision;
        N.weights[i] -= derivPrecision;
        deriv.push_back(partDeriv);
    }
    return deriv;
}

// Returns  the network's cost function partial derivative acording to a neurons bias
double neuralNet::getNeuronBDeriv(neuron &N, vector<double> input, vector<double> desired_out) const
{
    vector<double> output = this->predict(input);
    double partDeriv;

    partDeriv = 0;

    N.bias += derivPrecision;
    partDeriv = (this->getCost(this->predict(input), desired_out) - this->getCost(output, desired_out)) / derivPrecision;
    N.bias -= derivPrecision;

    return partDeriv;
}

// Gradient descent algorithm, trains the network with an input and the desired output
void neuralNet::train(vector<double> input, vector<double> desired_out)
{
    for (int i = neuronMatrix.size() - 1; i >= 1; i--) //not counting 1st layer, its only for inputs and should not me optimized
    {
        for (auto &neuron : neuronMatrix[i]) //for all the neurons in this layer
        {
            double bCorrection = getNeuronBDeriv(*neuron, input, desired_out) * beta; //get the partial derivatives
            vector<double> wCorrection = getNeuronWDeriv(*neuron, input, desired_out);
            for (auto &w : wCorrection)
            {
                w *= beta; //adjust learning rate beta, which should be close to 1.
            }
            //Since the partial derivative gives de direction of biggest increase in the cost function, subtracting the partial derivative vector from the weights vector, minimizes the cost function
            neuron->weights = neuron->weights - wCorrection;
            neuron->bias = neuron->bias - bCorrection;
        }
    }
}

//the chosen activation function, hard-coded, for now
double sigmoid(double x)
{
    return 1.0 / (1 + exp(-x));
}

// Protocol for saving a networg to a file
void neuralNet::save(string s)
{
    fstream file;
    file.open(s, ios::out | ios::binary);
    unsigned int layers = this->neuronsByLayer.size();
    unsigned int weightsSize;
    if (file.is_open())
    {
        file.write((char *)&layers, sizeof(unsigned int));
        file.write((char *)(&this->neuronsByLayer[0]), sizeof(int) * this->neuronsByLayer.size());
        file.write((char *)(&this->beta), sizeof(double));
        file.write((char *)(&this->derivPrecision), sizeof(double));

        for (int i = 0; i < int(this->neuronMatrix.size()); i++)
        {
            for (int j = 0; j < int(this->neuronMatrix[i].size()); j++)
            {
                weightsSize = this->neuronMatrix[i][j]->getWeightsRef().size();
                file.write((char *)&weightsSize, sizeof(unsigned int));
                file.write((char *)&this->neuronMatrix[i][j]->getWeightsRef()[0], weightsSize * sizeof(double));
                file.write((char *)&this->neuronMatrix[i][j]->getBiasRef(), sizeof(double));
            }
        }
        file.close();
    }
}
//Protocol for loading a network from a file
void neuralNet::load(string s)
{
    fstream file;
    vector<weak_ptr<neuron>> prevConVBuffer;
    vector<int> neuByLayBuffer;
    unsigned int weightsSizeBuffer;
    vector<double> weightsBuffer;
    double biasBuffer;
    unsigned int layers;
    file.open(s, ios::in | ios::binary);
    if (file.is_open())
    {
        file.read((char *)&layers, sizeof(unsigned int));
        this->neuronsByLayer.resize(layers);
        file.read((char *)&this->neuronsByLayer[0], layers * sizeof(int));
        file.read((char *)&(this->beta), sizeof(double));
        file.read((char *)&(this->derivPrecision), sizeof(double));

        this->neuronMatrix.clear();
        this->neuronMatrix.resize(layers);

        for (int i = 0; i < int(this->neuronsByLayer.size()); i++)
        {
            for (int j = 0; j < this->neuronsByLayer[i]; j++)
            {
                if (i != 0)
                {
                    prevConVBuffer.clear();
                    for (int k = 0; k < int(this->neuronMatrix[i - 1].size()); k++)
                    {
                        prevConVBuffer.emplace_back(neuronMatrix[i - 1][k]);
                    }
                }
                file.read((char *)&weightsSizeBuffer, sizeof(unsigned int));
                weightsBuffer.resize(weightsSizeBuffer);
                file.read((char *)&weightsBuffer[0], weightsSizeBuffer * sizeof(double));
                file.read((char *)&biasBuffer, sizeof(double));
                this->neuronMatrix[i].push_back(shared_ptr<neuron>(new neuron(weightsBuffer, biasBuffer, prevConVBuffer, sigmoid)));
            }
        }
        file.close();
    }
}

// All a layer's neuron to string, for debugging
string neuralNet::layerToString(int l) const
{
    string s = "Layer " + to_string(l) + ':' + '\n';
    for (auto &neuron : neuronMatrix[l])
    {
        s += neuron->toString() + '\n';
    }
    return s;
}

// Gets the number o layers in a network
int neuralNet::getLayerCount() const
{
    return neuronsByLayer.size();
}

// This just calls the neuron::breed for all neurons
void neuralNet::breed(neuralNet &net1, neuralNet &net2)
{
    for (int i = 0; i < int(this->neuronMatrix.size()); i++)
    {
        for (int j = 0; j < int(this->neuronMatrix[i].size()); j++)
        {
            this->neuronMatrix[i][j]->breed(*net1.neuronMatrix[i][j], *net2.neuronMatrix[i][j]);
        }
    }
}

// The breed function for my Genetic Algotihm, it just has a 50/50 chance of choosing each weight/bias from a parent, overwriting the child's wheigts/bias
void neuron::breed(neuron &neuron1, neuron &neuron2)
{
    for (int i = 0; i < int(this->weights.size()); i++)
    {
        if (rand() % 2)
        {
            this->weights[i] = neuron1.weights[i];
        }
        else
        {
            this->weights[i] = neuron2.weights[i];
        }
    }
    if (rand() % 2)
    {
        this->bias = neuron1.bias;
    }
    else
    {
        this->bias = neuron2.bias;
    }
    //mutations
    for (auto &d : this->weights)
    {
        if (!rand() % 10)
            d += randDouble(-15, 15, 8);
    }
    if (!rand() % 10)
        this->bias += randDouble(-50, 50, 8);
}

// This exports the C++ functions as C function to be called from python's ctypes module
extern "C"
{
    neuralNet *neuralNet_new(int *neuByLay, int neuByLaySize, double derivPrecision, double beta, double wLowBound, double wHighBound, double wDecPlaces, double bLowBound, double bHighBound, double bDecPlaces)
    {
        return new neuralNet(vector<int>(neuByLay, neuByLay + neuByLaySize), sigmoid, derivPrecision, beta, wLowBound, wHighBound, wDecPlaces, bLowBound, bHighBound, bDecPlaces);
    }
    void neuralNet_predict(neuralNet *net, double *input, int inputSize, double *writeOuputHere)
    {
        vector<double> inputV = vector<double>(input, input + inputSize);
        //cout << "input Vector in .cpp:" << vectorToString(inputV) << endl;
        vector<double> result = net->predict(inputV);
        //cout << "output Vector in .cpp:" << vectorToString(result) << endl;
        for (int i = 0; i < int(result.size()); i++)
        {
            writeOuputHere[i] = result[i];
        }
        return;
    }
    void neuralNet_train(neuralNet *net, double *input, int inputSize, double *expect, int expectSize)
    {
        vector<double> inputV = vector<double>(input, input + inputSize);
        vector<double> outputV = vector<double>(expect, expect + expectSize);
        //cout << vectorToString(inputV) << endl;
        //cout << vectorToString(outputV) << endl;
        net->train(inputV, outputV);
    }

    double neuralNet_getCost(neuralNet *net, double *output, int outputSize, double *expect, int expectSize)
    {
        vector<double> outputV = vector<double>(output, output + outputSize);
        vector<double> expectV = vector<double>(expect, expect + expectSize);
        // cout << vectorToString(outputV) << endl;
        // cout << vectorToString(expectV) << endl;
        return net->getCost(outputV, expectV);
    }

    void neuralNet_delete(neuralNet *net)
    {
        delete net;
    }

    void neuralNet_save(neuralNet *net, const char *filename)
    {
        net->save(filename);
    }

    void neuralNet_load(neuralNet *net, const char *filename)
    {
        net->load(filename);
    }

    void neuralNet_breed(neuralNet *net, neuralNet *net1, neuralNet *net2)
    {
        net->breed(*net1, *net2);
    }
}
