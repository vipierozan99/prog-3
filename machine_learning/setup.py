from setuptools import setup, Extension

# run: pyhton3 setup.py build

# Compile mysum.cpp into a shared library
setup(
    # ...
    ext_modules=[Extension('MLKit', ['MLKit.cpp'], extra_compile_args = ["-std=c++11"], ), ],
)
