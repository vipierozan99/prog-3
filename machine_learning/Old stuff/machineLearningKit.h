#include "matrixClass.h"

using namespace std;

class neuron
{
	bool hasCalculated;
	matrix *weights;
	double bias;
	double lastGetActValue;
	double (*actFunc)(double);
	int prevConC;
	neuron **prevConV;

public:
	neuron(matrix *w, double b, neuron **prevConV, int prevConC, double (*actFunc)(double))
	{
		this->weights = w;
		this->bias = b;
		this->prevConV = prevConV;
		this->prevConC = prevConC;
		this->actFunc = actFunc;
		this->lastGetActValue = 0;
		this->hasCalculated = false;
	}

	~neuron()
	{
		if (weights)
			delete weights; //?? maybe...
	}

	void setWeights(matrix *w)
	{
		weights = w;
		hasCalculated = false;
		return;
	}

	void setWeight(double w, int index)
	{
		(*weights)[0][index] = w;
		hasCalculated = false;
		return;
	}

	matrix *getWeights()
	{
		return weights;
	}
	double getWeight(int index)
	{
		return (*weights)[0][index];
	}

	void setBias(double b)
	{
		bias = b;
		hasCalculated = false;
		return;
	}

	double getBias()
	{
		return bias;
	}

	double getLastValue()
	{
		return lastGetActValue;
	}
	bool hasCalc()
	{
		return hasCalculated;
	}

	bool allPrevsHaveCalc()
	{
		for (int i = 0; i < prevConC; i++)
		{
			if (!this->hasCalculated || !prevConV[i]->allPrevsHaveCalc())
			{
				return false;
			}
		}
		return true;
	}

	double getActValue()
	{
		if (prevConC == 0)
		{
			lastGetActValue = actFunc(bias);
			return lastGetActValue;
		}
		// if (this->allPrevsHaveCalc() && this->hasCalculated) // && and prevCons have calculated
		// {
		// 	return lastGetActValue;
		// }
		matrix *prevActValues = new matrix(prevConC, 1); //delete
		for (int i = 0; i < prevConC; i++)
		{
			(*prevActValues)[i][0] = prevConV[i]->getActValue();
		}

		matrix *actFuncInput = weights->matMult(prevActValues); //delete

		lastGetActValue = actFunc((*actFuncInput)[0][0] + bias);

		delete prevActValues;
		delete actFuncInput;

		hasCalculated = 1;
		return lastGetActValue;
	}
};

class neuralNet
{
	static double getNeuronBias(double x) //using the same getActValue call to get neuron bias in first layer, for recursion purposes
	{
		//if neuron has no prev connects it calls this with x=bias
		return x;
	}

public:
	int *neuronsByLayer;
	int layers;
	//double *inputV; //first layer neurons will be set to have bias=input
	matrix *outputV;
	double (*actFunc)(double);
	neuron ***neuronMatrix; // vector of vectors of ptrs(neurons)

	neuralNet(int *neuronsByLayer, int layers, double (*actFunc)(double))
	{
		//this->inputV = inputV;
		this->outputV = new matrix(1, neuronsByLayer[layers - 1]);
		this->neuronsByLayer = neuronsByLayer;
		this->layers = layers;
		this->actFunc = actFunc;
		this->neuronMatrix = new neuron **[layers];

		matrix *weights;
		for (int i = 0; i < 1; i++)
		{
			neuronMatrix[i] = new neuron *[neuronsByLayer[i]];
			for (int j = 0; j < neuronsByLayer[i]; j++)
			{
				//converting *double input to essentially neuron *input, since getNeuronBias just returns the arg.
				//first layer is bias=input, weight irrelevant
				//                                2nd 0 = inputV[j]
				neuronMatrix[i][j] = new neuron(NULL, 0, NULL, 0, this->getNeuronBias);
			}
		}
		for (int i = 1; i < layers - 1; i++)
		{
			neuronMatrix[i] = new neuron *[neuronsByLayer[i]];
			for (int j = 0; j < neuronsByLayer[i]; j++)
			{
				weights = new matrix(1, neuronsByLayer[i - 1]); //weights size==prevConC
				weights->initRand(-1, 1);
				neuronMatrix[i][j] = new neuron(weights, 0, neuronMatrix[i - 1], neuronsByLayer[i - 1], actFunc);
			}
		}
		for (int i = layers - 1; i < layers; i++)
		{
			neuronMatrix[i] = new neuron *[neuronsByLayer[i]];
			for (int j = 0; j < neuronsByLayer[i]; j++)
			{
				weights = new matrix(1, neuronsByLayer[i - 1]);
				weights->initRand(-1, 1);
				neuronMatrix[i][j] = new neuron(weights, 0, neuronMatrix[i - 1], neuronsByLayer[i - 1], actFunc);
			}
		}
	}

	~neuralNet()
	{
		for (int i = 0; i < layers; i++)
		{
			for (int j = 0; j < neuronsByLayer[i]; j++)
			{
				delete neuronMatrix[i][j];
			}
			delete[] neuronMatrix[i];
		}
		delete[] neuronMatrix;
		delete outputV;
	}

	matrix *getOutputV()
	{
		return outputV;
	}
	void predict(matrix *input)
	{
		for (int i = 0; i < neuronsByLayer[0]; i++)
		{
			neuronMatrix[0][i]->setBias((*input)[0][i]);
		}

		for (int i = 0; i < neuronsByLayer[layers - 1]; i++)
		{
			(*outputV)[0][i] = neuronMatrix[layers - 1][i]->getActValue();
		}
	}

	string toString()
	{
		string s;
		for (int i = 0; i < layers; i++)
		{
			for (int j = 0; j < neuronsByLayer[i] - 1; j++)
			{
				s += to_string(neuronMatrix[i][j]->getLastValue()) + string(" // ");
			}
			s += to_string(neuronMatrix[i][neuronsByLayer[i] - 1]->getLastValue()) + string(" \n");
		}
		return s;
	}
	string hasCalcToString()
	{
		string s;
		for (int i = 0; i < layers; i++)
		{
			for (int j = 0; j < neuronsByLayer[i] - 1; j++)
			{
				s += to_string(neuronMatrix[i][j]->hasCalc()) + string(" // ");
			}
			s += to_string(neuronMatrix[i][neuronsByLayer[i] - 1]->hasCalc()) + string(" \n");
		}
		return s;
	}

	string outToString()
	{
		string s; //= string("output:");
		for (int i = 0; i < neuronsByLayer[layers - 1] - 1; i++)
		{
			s += to_string((*this->outputV)[0][i]) + string(",");
		}
		s += to_string((*this->outputV)[0][neuronsByLayer[layers - 1] - 1]) + string("\n");
		return s;
	}
	double getCost(matrix *input, matrix *target)
	{
		if (target->getCols() != neuronsByLayer[layers - 1])
		{
			cerr << "targetVector must be same size as last neuron layer" << endl;
			return 0;
		}
		this->predict(input);
		matrix *resultMroot = target->matSub(this->outputV); // need to pow (x,2)
		matrix *resultM = resultMroot->scalarToPower(2);

		double result = resultM->getSumAllel(); // sum all here
		delete resultM;
		delete resultMroot;
		return result;
	}

	matrix *getNeuronWeightParcialDeriv(neuron *neur, matrix *input, matrix *target, double h)
	{
		matrix *resultM = new matrix(1, neur->getWeights()->getCols());
		double result;
		for (int i = 0; i < resultM->getCols(); i++)
		{
			result = 0;
			result -= this->getCost(input, target);
			neur->setWeight(neur->getWeight(i) + h, i);
			result += this->getCost(input, target);
			neur->setWeight(neur->getWeight(i) - h, i);
			result /= h;
			(*resultM)[0][i] = result;
		}
		return resultM;
	}

	double getNeuronBiasParcialDeriv(neuron *neur, matrix *input, matrix *target, double h)
	{
		double result;
		result = 0;
		result -= this->getCost(input, target);
		neur->setBias(neur->getBias() + h);
		result += this->getCost(input, target);
		neur->setBias(neur->getBias() - h);
		result /= h;

		return result;
	}

	double minimize(matrix *input, matrix *target)
	{
		if (target->getCols() != neuronsByLayer[layers - 1])
		{
			cerr << "targetVector must be same size as last neuron layer" << endl;
			return 0;
		}
		double h = 0.0000000000001;
		//go up from neural net, output to input;

		for (int i = layers - 1; i >= 1; i--) //not counting 1st layer because its input should not be minimized
		{
			for (int j = 0; j < neuronsByLayer[i]; j++)
			{
				matrix *previousWeights = neuronMatrix[i][j]->getWeights();
				matrix *weightNabla = this->getNeuronWeightParcialDeriv(neuronMatrix[i][j], input, target, h);
				neuronMatrix[i][j]->setWeights(previousWeights->matSub(weightNabla));
				neuronMatrix[i][j]->setBias(neuronMatrix[i][j]->getBias() - this->getNeuronBiasParcialDeriv(neuronMatrix[i][j], input, target, h));
				delete previousWeights;
				delete weightNabla;
			}
		}

		return this->getCost(input, target);
	}
};