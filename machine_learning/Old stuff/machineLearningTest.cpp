#include "machineLearningKit.h"
#include <iostream>
#include <math.h>
#include <fstream>
using namespace std;

void cleanUp(matrix **vector, int size)
{
    for (int i = 0; i < size; i++)
    {
        if (vector[i] != NULL)
        {
            delete vector[i];
        }
    }
    delete[] vector;
}

double sigmoid(double x)
{
    return 1.0 / (1 + exp(-x));
}

matrix *get0to10fromDouble(double x)
{
    if (x > 8 || x < 3)
    {
        cerr << "3<=x<=8" << endl;
        return NULL;
    }
    matrix *result = new matrix(1, 6);
    result->initZero();
    (*result)[0][(int)x - 3] = 1;
    return result;
}

int main(int argc, char const *argv[])
{

    int layers = 5;
    int neuronsByLayer[layers] = {11, 5, 5, 5, 6};
    neuralNet myNet = neuralNet(neuronsByLayer, layers, sigmoid);
    cout << myNet.toString() << endl;

    fstream ip("winequality-red.csv");
    string s;
    getline(ip, s);
    int line = 0;
    int column = 0;

    matrix **trainingData = new matrix *[800];
    matrix **target = new matrix *[800];
    for (int i = 0; i < 800; i++)
    {
        trainingData[i] = NULL;
        target[i] = NULL;
    }
    string delimiter;
    size_t pos;
    string token;
    while (line <700 && ip.good())
    {
        getline(ip, s);
        //cout << s << endl;
        delimiter = ";";
        pos = 0;

        trainingData[line] = new matrix(1, 11);
        while ((pos = s.find(delimiter)) != string::npos)
        {
            token = s.substr(0, pos);
            (*trainingData[line])[0][column] = stod(token);
            s.erase(0, pos + delimiter.length());
            column++;
        }
        target[line] = get0to10fromDouble(stod(s));
        column = 0;
        //cout << "training inputs:" << trainingData[line]->toString() << endl;
        //cout << "training outputs:" << target[line]->toString() << endl;
        line++;
    }

    for (int i = 0; i < 100; i++)
    {
        cout << "training inputs:" << trainingData[i]->toString() << endl;
        cout << "training outputs:" << target[i]->toString() << endl;
    }

    for (int i = 0; i < 30; i++) // training
    {
        cout << "training with:" << trainingData[i]->toString() << endl
             << "target is:" << target[i]->toString() << endl;
        myNet.minimize(trainingData[i], target[i]);
        cout << "output was:" << myNet.getOutputV()->toString() << endl
             << endl;
        cout << "cost was:" << myNet.getCost(trainingData[i], target[i]) << endl;
    }

    for (int i = 400; i < 430; i++)
    {
        myNet.predict(trainingData[i]);
        cout << "prediction:" << myNet.getOutputV()->toString() << endl
             << "answer:    " << target[i]->toString() << endl;
	cout << "cost was:  " << myNet.getCost(trainingData[i], target[i]) << endl;
    }

    cleanUp(trainingData, 800);
    cleanUp(target, 800);
    return 0;
}
