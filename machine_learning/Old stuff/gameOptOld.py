import pygame
import random
from pygame.locals import *
from operator import itemgetter

#from mem_top import mem_top

import MLKitWrapper as ml


class Player(pygame.sprite.Sprite):
    def __init__(self, brain, idNum):
        super(Player, self).__init__()
        self.surf = pygame.Surface((25, 25))
        self.surf.fill((random.randint(0, 255), random.randint(
            0, 255), random.randint(0, 255)))
        #self.surf = pygame.image.load("lala.jpg")
        self.rect = self.surf.get_rect(center=(40, 400))
        self.speed = [0, 0]
        self.accel = [0, -2]
        self.brain = brain
        self.idNum = idNum

    def jump(self, impulsePower):
        if self.rect.bottom >= 500:
            self.speed[1] = impulsePower*30

    def update(self, pressed_keys, MLData):
        self.speed[0] += self.accel[0]
        self.speed[1] += self.accel[1]
        self.rect.move_ip(-self.speed[0], -self.speed[1])

        if pressed_keys[K_UP]:
            self.jump(1)

        # print(self.brain.predict([MLData["Xcoord"],MLData["Ycoord"]])[0])
        shouldJump = self.brain.predict(
            [MLData["Xcoord"], MLData["Ycoord"]])[0]

        if shouldJump > 0.2:  # cant jump in air, so this is ok
            self.jump(shouldJump)

        # if pressed_keys[K_LEFT]:
            #self.rect.move_ip(-10, 0)
        # if pressed_keys[K_RIGHT]:
            #self.rect.move_ip(10, 0)

        if self.rect.left < 0:
            self.rect.left = 0
        elif self.rect.right > 800:
            self.rect.right = 800
        if self.rect.top <= 0:
            self.rect.top = 0
        elif self.rect.bottom >= 500:
            self.rect.bottom = 500


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super(Enemy, self).__init__()
        self.height = random.randint(30, 75)
        self.surf = pygame.Surface((20, self.height))
        self.surf.fill((255, 255, 255))
        self.rect = self.surf.get_rect(
            center=(random.randint(800, 950), 500-(self.height/2))
        )
        self.speed = -10

    def update(self):
        self.rect.move_ip(self.speed, 0)
        if self.rect.right < 0:
            self.kill()


class ScreenText():
    def __init__(self, text, x, y):
        self.font = pygame.font.Font('freesansbold.ttf', 32)
        self.text = self.font.render(text, True, (255, 255, 255), (0, 0, 0))
        self.textRect = self.text.get_rect()
        self.textRect.center = (x//2, y//2)

    def update(self, text):
        self.text = self.font.render(text, True, (255, 255, 255), (0, 0, 0))

# create a text suface object,
# on which text is drawn on it.


# create a rectangular object for the
# text surface object
# print(mem_top())

pygame.init()
FPS = 75
fpsclock = pygame.time.Clock()

screen = pygame.display.set_mode((800, 600))

ADDENEMY = pygame.USEREVENT + 1
initialFreq = 40000
pygame.time.set_timer(ADDENEMY, int(initialFreq/FPS))

score = 0
individuals = []
generationSize = 10
fitnessVector = [0]*generationSize

for i in range(generationSize):
    ind = ml.neuralNet([2, 2, 2], i)
    individuals.append(Player(ind, i))


ground = pygame.Surface((800, 100))
ground.fill((0, 255, 0))

enemies = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
players = pygame.sprite.Group()
generation = 0

difficulty = ScreenText("Difficulty: 100%", 255, 50)
scoreDisplay = ScreenText("Score: 0", 130, 120)
generationDisplay = ScreenText("Generation: 0", 220, 190)
exit = False

while True:
    score = 0
    difficulty.update("Difficulty: 100%")
    scoreDisplay.update("Score: 0")
    pygame.time.set_timer(ADDENEMY, int(initialFreq/FPS))
    print("Generation:", generation)
    generationDisplay.update("Generation: "+str(generation))

    for i in range(generationSize):
        all_sprites.add(individuals[i])
        players.add(individuals[i])

    MLData = {
        "Ycoord": 0,
        "Xcoord": 0
    }

    running = True
    if exit:
        break
    while running:
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
                    exit = True
            elif event.type == QUIT:
                running = False
            elif(event.type == ADDENEMY):
                new_enemy = Enemy()
                enemies.add(new_enemy)
                all_sprites.add(new_enemy)

        enemies.update()

        # print(all_sprites.sprites())

        for enemy in enemies.sprites():
            if enemy.rect.left > 40 and enemy.rect.left == min([y.rect.left for y in enemies.sprites()]):
                MLData["Xcoord"] = enemy.rect.left
                MLData["Ycoord"] = enemy.rect.top

        pressed_keys = pygame.key.get_pressed()

        for player in players.sprites():
            player.update(pressed_keys, MLData)

        # print(MLData)
        screen.fill((0, 0, 0))
        screen.blit(ground, (0, 500))
        screen.blit(player.surf, player.rect)
        screen.blit(difficulty.text, difficulty.textRect)
        screen.blit(scoreDisplay.text, scoreDisplay.textRect)
        screen.blit(generationDisplay.text, generationDisplay.textRect)

        for entity in all_sprites:
            screen.blit(entity.surf, entity.rect)

        for player in players.sprites():
            if pygame.sprite.spritecollideany(player, enemies):
                print("colision! score:", score)
                fitnessVector[player.idNum] = score
                player.kill()

        if not bool(players):
            running = False

        score += 1
        scoreDisplay.update("Score: "+str(score))

        if score % 300 == 0:
            pygame.time.set_timer(ADDENEMY, int(initialFreq/FPS)-int(score/10))
            print("score", score)
            print("speed", '%.3f' % (int(initialFreq*100/FPS) /
                                     (int(initialFreq/FPS)-int(score/10))), "%")
            difficulty.update("Difficulty: "+'%.3f' %
                              (initialFreq*100/(initialFreq-int(score/10)))+"%")

        pygame.display.flip()

        fpsclock.tick(FPS)

    for entity in all_sprites:
        entity.kill()

    individuals = list(zip(individuals, fitnessVector))  # sort vector
    individuals.sort(key=itemgetter(1), reverse=True)
    offspringBrains = []
    for i in range(generationSize):
        offspringBrainsBuf = ml.neuralNet([2, 2, 2], i)
        if(i < generationSize/2 and i != 0):
            offspringBrainsBuf.breed(
                individuals[0][0].brain.obj, offspringBrainsBuf.obj)
        if(i == 0):
            offspringBrainsBuf = individuals[0][0].brain

        offspringBrains.append(offspringBrainsBuf)

    # bestIndividual = individuals[0][0]
    print(individuals)
    individuals = []
    # individuals.append(bestIndividual)
    for i in range(generationSize):
        individuals.append(Player(offspringBrains[i], i))
    fitnessVector = [0]*generationSize
    generation += 1

    # print(mem_top())

    # void neuralNet::breed(neuralNet& neuralNet)
    # keep new 1st, clear individuals list, reinitialize it


# print(mem_top())
