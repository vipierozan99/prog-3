#include "machineLearningKitOld.h"
#include <iostream>
#include <math.h>
using namespace std;

double sigmoid(double x)
{
    return 1.0 / (1 + exp(-x));
}

double testFunc(double x, double y)
{
    return (pow(x, 2) + y);
}

int main(int argc, char const *argv[])
{

    int layers = 3;
    int neuronsByLayer[layers] = {3, 2, 2};
    matrix *output;
    double input[neuronsByLayer[0]] = {0.3, 0.3, 0.3};
    matrix *target = new matrix(1, neuronsByLayer[layers - 1]);
    for (int i = 0; i < target->getCols(); i++)
    {
        target[0][i] = 0;
    }

    neuralNet myNet = neuralNet(input, neuronsByLayer, layers, sigmoid);
    cout << myNet.toString() << endl;
    cout << myNet.hasCalcToString() << endl;
    myNet.predict();
    output = myNet.getOutputV();
    cout << "output:" << output->toString() << endl;

    for (int i = 0; i < 14; i++)
    {

        cout << string("cost:") << myNet.minimize(target) << endl;
        cout << myNet.toString() << endl;
    }

    delete output;
    delete target;

    return 0;
}
