#ifndef dLinkL
#define dLinkL

#include <string>
#include <typeinfo>

using namespace std;

template<typename dataType>
class dLinkListNode
{

    dLinkListNode *next;
    dLinkListNode *previous;
    dataType value;

public:
    dLinkListNode(dLinkListNode *p, dataType v, dLinkListNode *n)
    {
        previous = p;
        value = v;
        next = n;
    }

    int &getValue()
    {
        return value;
    }
    dLinkListNode *getNext()
    {
        return next;
    }
    dLinkListNode *getPrevious()
    {
        return previous;
    }
	template<dataType>
    void setValue(dataType a)
    {
        value = a;
    }
	template<dataType>
    void operator=(dataType v)
    {
        return setValue(v);
    }

    void setNext(dLinkListNode *a)
    {
        next = a;
    }
    void setPrevious(dLinkListNode *a)
    {
        previous = a;
    }
};

template<typename dataType>
class dLinkList
{
    dLinkListNode *first;
    dLinkListNode *last;
    int size;

    dLinkListNode &getNode(int index)
    {
        dLinkListNode *pointer = first;
        for (int i = 0; i < index; i++)
        {
            pointer = pointer->getNext();
        }
        return *pointer;
    }
    dLinkListNode *getNodePtr(int index)
    {
        dLinkListNode *pointer = first;
        for (int i = 0; i < index; i++)
        {
            pointer = pointer->getNext();
        }
        return pointer;
    }

public:
    dLinkList()
    {
        this->first = NULL;
        this->last = NULL;
        this->size = 0;
    }
    ~dLinkList()
    {
        while (size > 0)
            this->removeIndex(0);
    }
    int getSize()
    {
        return size;
    }
	template<dataType>
    int &operator[](int index)
    {

        return getNode(index).getValue();
    }

	template<dataType>
    int append(dataType v)
    {
        dLinkListNode *buf = new dLinkListNode(last, v, NULL);
        if (last)
            last->setNext(buf);
        last = buf;
        if (!first)
            first = last;
        size++;
        return size - 1;
    }
	
	template<dataType>
    int prepend(dataType v)
    {
        dLinkListNode *buf = new dLinkListNode(NULL, v, first);
        if (first)
            first->setPrevious(buf);
        first = buf;
        if (!last)
            last = first;
        size++;
        return size - 1;
    }

    int removeIndex(int index)
    {
        dLinkListNode *nodePtr = getNodePtr(index);
        if (nodePtr->getPrevious())
            nodePtr->getPrevious()->setNext(nodePtr->getNext());
        else
            this->first = nodePtr->getNext();
        if (nodePtr->getNext())
            nodePtr->getNext()->setPrevious(nodePtr->getPrevious());
        else
            this->last = nodePtr->getPrevious();
        int ret = nodePtr->getValue();
        delete nodePtr;
        size--;
        return ret;
    }

    string toString()
    {
        string s;
        s += string("[");

        dLinkListNode *pointer = first;
        for (int i = 1; i < (size - 1) * 2; i += 2)
        {
            s += to_string(pointer->getValue()) + string(",");
            pointer = pointer->getNext();
        }
        s += to_string(pointer->getValue()) + string("]");
        return s;
    }
};

#endif
