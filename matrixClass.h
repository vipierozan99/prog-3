#include <string>
#include <iostream>
#include <random>
#include <math.h>
using namespace std;

class matrix
{
    int rows;
    int cols;
    double **data;

public:
    matrix(int rows, int cols)
    {
        this->rows = rows;
        this->cols = cols;
        this->data = new double *[rows];
        for (int i = 0; i < rows; ++i)
            this->data[i] = new double[cols];
    }
    ~matrix()
    {
        for (int i = 0; i < this->rows; ++i)
            delete[] this->data[i];
        delete[] this->data;
    }

    int getRows()
    {
        return rows;
    }
    int getCols()
    {
        return cols;
    }

    string toString()
    {
        string s;
        if (!this)
        {
            return string("[this is null]");
        }
        s += string("[");

        for (int i = 0; i < this->rows - 1; i++)
        {
            for (int j = 0; j < (this->cols) - 1; j++)
            {
                s += to_string(this->data[i][j]) + string(",");
            }
            s += to_string(this->data[i][(this->cols) - 1]) + string("\n");
        }
        for (int j = 0; j < (this->cols) - 1; j++)
        {
            s += to_string(this->data[this->rows - 1][j]) + string(",");
        }
        s += to_string(this->data[this->rows - 1][(this->cols) - 1]) + string("]");

        return s;
    }

    double *&operator[](int i)
    {
        if (i > rows || i < 0)
        {
            cerr << "invalid read, 0<=i<" + rows << endl;
        }
        return data[i];
    }
    matrix *scalarToPower(double power)
    {
        matrix *result = new matrix(this->rows, this->cols);
        for (int i = 0; i < (this->rows); i++)
        {
            for (int j = 0; j < (this->cols); j++)
            {
                (*result)[i][j] = pow(this->data[i][j], power);
            }
        }
        return result;
    }

    matrix *scalarMult(double x)
    {
        matrix *result = new matrix(this->rows, this->cols);
        for (int i = 0; i < (this->rows); i++)
        {
            for (int j = 0; j < (this->cols); j++)
            {
                (*result)[i][j] = this->data[i][j] * x;
            }
        }
        return result;
    }
    matrix *scalarSum(double x)
    {
        matrix *result = new matrix(this->rows, this->cols);
        for (int i = 0; i < (this->rows); i++)
        {
            for (int j = 0; j < (this->cols); j++)
            {
                (*result)[i][j] = this->data[i][j] + x;
            }
        }
        return result;
    }

    double getSumAllel()
    {
        double result = 0;
        for (int i = 0; i < (this->rows); i++)
        {
            for (int j = 0; j < (this->cols); j++)
            {
                result += this->data[i][j];
            }
        }
        return result;
    }

    matrix *matSum(matrix *B)
    {
        if (rows != B->getRows() || cols != B->getCols())
        {
            cerr << "matrix A and B must be the same size" << endl;
            return NULL;
        }

        matrix *result = new matrix(this->rows, B->getCols());
        for (int i = 0; i < (this->rows); i++)
        {
            for (int j = 0; j < (B->getCols()); j++)
            {
                (*result)[i][j] = this->data[i][j] + (*B)[i][j];
            }
        }
        return result;
    }
    matrix *matSub(matrix *B)
    {
        if (rows != B->getRows() || cols != B->getCols())
        {
            cerr << "matrix A and B must be the same size" << endl;
            return NULL;
        }

        matrix *result = new matrix(this->rows, B->getCols());
        for (int i = 0; i < (this->rows); i++)
        {
            for (int j = 0; j < (B->getCols()); j++)
            {
                (*result)[i][j] = this->data[i][j] - (*B)[i][j];
            }
        }
        return result;
    }

    matrix *matMult(matrix *B)
    {
        if (cols != B->getRows())
        {
            cerr << "matrixA cols must equal masrtixB rows" << endl;
            return NULL;
        }
        matrix *result = new matrix(this->rows, B->getCols());
        for (int i = 0; i < (this->rows); i++)
        {
            for (int j = 0; j < (B->getCols()); j++)
            {
                (*result)[i][j] = 0;
                for (int k = 0; k < (this->cols); k++)
                {
                    (*result)[i][j] += this->data[i][k] * (*B)[k][j];
                }
            }
        }
        return result;
    }

    matrix *transpose()
    {
        matrix *result = new matrix(cols, rows);
        for (int i = 0; i < (rows); i++)
        {
            for (int j = 0; j < (cols); j++)
            {
                (*result)[j][i] = this->data[i][j];
            }
        }
        return result;
    }

    void initRand(double min, double max)
    {
        uniform_real_distribution<double> unif(min, max);
        default_random_engine re;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                this->data[i][j] = unif(re);
            }
        }
    }

    void initZero()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                this->data[i][j] = 0;
            }
        }
    }
};