#include <iostream>
#include <string>
#include <sstream>
#include <cmath>

using namespace std;

class point
{
public:
	int x;
	int y;

	point(int x = 0, int y = 0)
	{
		this->x = x;
		this->y = y;
	}

	double getNorm()
	{
		return sqrt(pow(this->x, 2) + pow(this->y, 2));
	}
};

ostream &operator<<(ostream &ss, const point &p)
{
	return ss << '(' << p.x << ',' << p.y << ')';
}

template <class dataType>
class list;

template <class dataType>
class node
{
	dataType data;
	node *next;
	node *prev;

public:
	friend class list<dataType>;

	node(dataType d, node *p = NULL, node *n = NULL)
	{
		this->next = n;
		this->data = d;
		this->prev = p;
	}

	void operator=(dataType &d)
	{
		this->data = d;
	}
};

template <class dataType>
class list
{
	node<dataType> *head;
	node<dataType> *tail;
	int size;

	bool insertEmpty(dataType d)
	{
		node<dataType> *n = new node<dataType>(d);
		if (!n)
			return false;
		this->head = n;
		this->tail = n;
		this->size++;
		return true;
	}

	dataType removeSingle()
	{
		delete this->head;
		this->head = NULL;
		this->tail = NULL;
		this->size--;
		return true;
	}

	dataType removeNode(node<dataType> *n)
	{
		if (size == 1)
			return removeSingle();
		if (n == this->head)
			return removeHead();
		if (n == this->tail)
			return removeTail();

		dataType retData = n->data;
		n->next->prev = n->prev;
		n->prev->next = n->next;
		this->size--;
		delete n;
		return retData;
	}

public:
	list()
	{
		this->size = 0;
		this->head = NULL;
		this->tail = NULL;
	}

	int getSize()
	{
		return this->size;
	}

	~list()
	{
		node<dataType> *aux = this->head;
		while (aux != NULL)
		{
			this->head = this->head->next;
			delete aux;
			aux = this->head;
		}
	}

	dataType &operator[](int index)
	{
		node<dataType> *aux = this->head;
		for (int i = 0; i < index; i++)
		{
			aux = aux->next;
		}
		return aux->data;
	}

	bool insertHead(dataType d)
	{
		if (!size)
			return insertEmpty(d);
		node<dataType> *newNode = new node<dataType>(d);
		if (!newNode)
			return false;
		newNode->next = this->head;
		this->head->prev = newNode;
		this->head = newNode;
		this->size++;
		return true;
	}

	dataType removeHead()
	{
		if (size == 1)
			return removeSingle();
		dataType retData = this->head->data;
		this->head = this->head->next;
		delete this->head->prev;
		this->head->prev = NULL;
		this->size--;
		return retData;
	}

	bool insertTail(dataType d)
	{
		if (!size)
			return insertEmpty(d);
		node<dataType> *newNode = new node<dataType>(d);
		if (!newNode)
			return false;
		newNode->prev = this->tail;
		this->tail->next = newNode;
		this->tail = newNode;
		this->size++;
		return true;
	}

	dataType removeTail()
	{
		if (size == 1)
			return removeSingle();
		dataType retData = this->tail->data;
		this->tail = this->tail->prev;
		delete this->tail->next;
		this->tail->next = NULL;
		this->size--;
		return retData;
	}

	bool insertBefore(node<dataType> *target, dataType d)
	{
		if (!size)
			return insertEmpty(d);
		if (target == this->head)
			return insertHead(d);
		if (target == this->tail)
			return insertTail(d);
		node<dataType> *newNode = new node<dataType>(d);
		if (!newNode)
			return false;

		newNode->next = target;
		newNode->prev = target->prev;
		newNode->prev->next = newNode;
		newNode->next->prev = newNode;
		this->size++;
		return true;
	}

	bool insertIndex(dataType d, int index)
	{
		if (!size)
			return insertEmpty(d);
		if (index <= 0)
			return insertHead(d);
		if (index >= this->size)
			return insertTail(d);
		node<dataType> *newNode = new node<dataType>(d);
		if (!newNode)
			return false;
		node<dataType> *aux = this->head;
		for (int i = 0; i < index; i++)
		{
			aux = aux->next;
		}
		newNode->next = aux;
		newNode->prev = aux->prev;
		newNode->prev->next = newNode;
		newNode->next->prev = newNode;
		this->size++;
		return true;
	}

	dataType removeIndex(int index)
	{
		if (size == 1)
			return removeSingle();
		if (index <= 0)
			return removeHead();
		if (index >= this->size)
			return removeTail();

		node<dataType> *aux = this->head;
		for (int i = 0; i < index; i++)
		{
			aux = aux->next;
		}
		dataType retData = aux->data;
		aux->next->prev = aux->prev;
		aux->prev->next = aux->next;
		delete aux;
		this->size--;
		return retData;
	}

	void mergeSort(int (*cmp)(dataType &a, dataType &b))
	{
		if (!cmp)
			return;
		if (this->size <= 1)
			return;

		node<dataType> *p, *q, *e, *tail, *oldhead, *list = this->head;
		int insize, nmerges, psize, qsize, i;

		insize = 1;

		while (1)
		{
			p = list;
			list = NULL;
			tail = NULL;

			nmerges = 0; /* count number of merges we do in this pass */

			while (p)
			{
				nmerges++; /* there exists a merge to be done */
				/* step `insize' places along from p */
				q = p;
				psize = 0;
				for (i = 0; i < insize; i++)
				{
					psize++;
					q = q->next;
					if (!q)
						break;
				}

				/* if q hasn't fallen off end, we have two lists to merge */
				qsize = insize;

				/* now we have two lists; merge them */
				while (psize > 0 || (qsize > 0 && q))
				{

					/* decide whether next element of merge comes from p or q */
					if (psize == 0)
					{
						/* p is empty; e must come from q. */
						e = q;
						q = q->next;
						qsize--;
					}
					else if (qsize == 0 || !q)
					{
						/* q is empty; e must come from p. */
						e = p;
						p = p->next;
						psize--;
					}
					else if (cmp(p->data, q->data) <= 0)
					{
						/* First element of p is lower (or same);
		     * e must come from p. */
						e = p;
						p = p->next;
						psize--;
					}
					else
					{
						/* First element of q is lower; e must come from q. */
						e = q;
						q = q->next;
						qsize--;
					}

					/* add the next element to the merged list */
					if (tail)
					{
						tail->next = e;
					}
					else
					{
						list = e;
					}
					e->prev = tail;
					tail = e;
				}

				/* now p has stepped `insize' places along, and q has too */
				p = q;
			}
			tail->next = NULL;

			/* If we have done only one merge, we're finished. */
			if (nmerges <= 1)
			{
				/* allow for nmerges==0, the empty list case */
				this->head = list;
				while (this->tail->next != NULL)
				{
					this->tail = this->tail->next;
				}
				return;
			}

			/* Otherwise repeat, merging lists twice the size */
			insize *= 2;
		}
	}

	string toString()
	{
		string s;
		stringstream ss(s);
		ss << '<';
		node<dataType> *aux = this->head;
		for (int i = 0; i < this->size; i++)
		{

			ss << aux->data << ',';
			aux = aux->next;
		}
		ss >> s;
		if (this->size)
			s.back() = '>';
		else
			s += '>';
		return s;
	}
};
