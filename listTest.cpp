#include "listaTemplate.hpp"
#include <iostream>
#include <string>
#include <random>

using namespace std;

int compare(point &a, point &b)
{
    if (a.getNorm() == b.getNorm())
        return 0;
    if (a.getNorm() > b.getNorm())
        return 1;
    if (a.getNorm() < b.getNorm())
        return -1;
}

int compare(int &a, int &b)
{
    if (a > b)
        return 1;
    if (a == b)
        return 0;
    if (a < b)
        return -1;
}

int main(int argc, char const *argv[])
{
    list<point> l = list<point>();

    srand(45);

    for (int i = 0; i < 60; i++)
    {
        l.insertHead(point(rand() % 100, rand() % 100));
    }

    cout << l.toString() << endl;

    for (int i = 0; i < l.getSize(); i++)
    {
        cout << l[i].getNorm() << endl;
    }

    l.mergeSort(compare);

    cout << l.toString() << endl;
    for (int i = 0; i < l.getSize(); i++)
    {
        cout << l[i].getNorm() << endl;
    }
    cout << l[0];

    return 0;
}
