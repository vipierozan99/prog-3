var myObj = {
	matriculas: ["18102012", "18205159"],
	counter: 0,
	addAluno: async function(id) {
		return new Promise((resolve) => {
			var matricula = document.getElementById("formAtendimento:maskMatricula");
			matricula.classList += " ui-state-focus";
			matricula.value = id;
			console.log("alunoid:", id);
			matricula.classList =
				"ui-inputfield ui-inputmask ui-widget ui-state-default ui-corner-al";
			PrimeFaces.ab({
				s: "formAtendimento:maskMatricula",
				e: "change",
				p: "formAtendimento:maskMatricula",
				u: "formAtendimento:matNomePanel",
				d: 100
			});
			console.log("aluno add");
			setTimeout(() => {
				resolve(this.getAlunoBtn());
			}, 1000);
		});
	},
	getAlunoBtn: async function() {
		return new Promise((resolve) => {
			if (
				document.getElementById("formAtendimento:btn_addAluno") != undefined
			) {
				document.getElementById("formAtendimento:btn_addAluno").click();
				setTimeout(() => {
					resolve(this.getAlunoBtn());
				}, 1000);
			} else {
				console.log("btn click");
				this.counter++;
				setTimeout(() => {
					resolve();
				}, 1000);
			}
		});
	}
};

var str2 = `18250307
17104124`;

const doIt = async () => {
	var matriculas = [...str2.split("\n")];
	for (i in matriculas) {
		const x = await myObj.addAluno(matriculas[i]);
		console.log(i, x);
	}
	return "done";
};

doIt();
