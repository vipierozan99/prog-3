#include <cstdio>

int numero_perfeito(int n)
{
    int soma = 0;

    for (int i = 1; i < n; i++)
    {
        if ((n % i) == 0)
        {
            soma += i;
        }
    }
    if (soma == n)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void encontra_perfeito(int min, int max, char *vec)
{
    int flag, n = 0;

    while (min <= max)
    {
        flag = numero_perfeito(min);

        if (flag == 1)
        {
            sprintf(vec, "%d", min);
            //printf("%d", min);
        }

        min++;
    }
}

int main()
{

    char vet[80];
    encontra_perfeito(1, 200, &vet[0]);

    printf("%s", vet);
}